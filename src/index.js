import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { InMemoryCache } from 'apollo-boost';
import { ApolloClient, HttpLink,ApolloProvider } from '@apollo/client';

const root = ReactDOM.createRoot(document.getElementById('root'));


const client = new ApolloClient({
  link: new HttpLink({
      uri: 'https://cors-anywhere.herokuapp.com/https://n7b67.sse.codesandbox.io/graphql',
      fetchOptions: {
         mode: 'no-cors', // no-cors, *cors, same-origin
      }
  }),
  cache: new InMemoryCache()
});

// const client = new ApolloClient({
//   uri: 'https://cors-anywhere.herokuapp.com/https://n7b67.sse.codesandbox.io/graphql',
//   cache:new InMemoryCache()
// });

root.render(
  
  <React.StrictMode>
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            News
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
    <ApolloProvider client={client}>
    <App/>
    </ApolloProvider>
    
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
