import './App.css';
import {React, useEffect, useState} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Container, Typography, Button } from '@mui/material';
import Box from '@mui/material/Box';
import { useQuery, gql } from '@apollo/client'

function App() {
  const [age, setAge] = useState('');
  const [movies, setMovies] = useState({});

  function createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }
  const GET_LOCATIONS = gql`
  query getMovies{
    movies {
    id
  	name
    genre
      actor {
        id
      }
  }
}
  


`;
const { loading, error, data } = useQuery(GET_LOCATIONS);
console.log(error)
console.log(data)
  const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
  ];

  return (
    <Container sx={{ mt: 10 }} maxWidth="lg">
      <Box sx={{display:"flex", justifyContent: 'space-between',alignItems: 'flex-start' }}>
        <Typography sx={{ mb: 2 }} variant="h5">List Movies</Typography>
        <Button sm variant="contained" >Add Movie</Button>
      </Box>
      
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name)</TableCell>
              <TableCell align="right">Genre</TableCell>
              <TableCell align="right">Age</TableCell>
              <TableCell align="right">Actors</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow
                key={row.name}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.calories}</TableCell>
                <TableCell align="right">{row.fat}</TableCell>
                <TableCell align="right">{row.carbs}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>

  );
}

export default App;
